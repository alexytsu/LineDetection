import cv2
import numpy as np

image_name = input("Enter the image name you want to analyse: ")
output_name = "output_" + image_name

img = cv2.imread(image_name))

hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
cv2.imwrite("hsv_" + image_name, hsv)

lower_pink = np.array([50, 50, 220])
upper_pink = np.array([254, 200, 255])

mask = cv2.inRange(img, lower_pink, upper_pink)
res = cv2.bitwise_and(img, img, mask = mask)
cv2.imwrite("pink_" + image_name, res)

edges = cv2.Canny(res, 50, 150)
cv2.imwrite("edges_" + image_name, edges)


lines = cv2.HoughLinesP(edges, 1, np.pi/180, 10)
for line in lines:
    x1, y1, x2, y2 = line[0]
    cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 10)

cv2.imwrite(output_name, img)