import cv2
import random
import numpy as np
import math

class Line():
    def __init__(self, height, width, image):

        possible = cv2.findNonZero(image)    
  
        self.x = possible[random.randrange(len(possible))][0][0]
        self.y = possible[random.randrange(len(possible))][0][1]

        #self.angle = 90
        self.angle = random.randrange(0, 180)
        self.fitness = 0
        self.image = image
        self.height = height
        self.width = width
    
    def print(self):
        #print(f"Line with x as {self.x} y as {self.y} and angle as {self.angle}")
        pass

    def getFitness(self):
        # print("===== Fitness calculations =====")
        fitness = 0

        if(abs(self.angle - 90) < 0.1):
            for y in range(self.height):
                
                if(self.x > 0 and self.x < self.width):
                    pixel = self.image[y, self.x]
                    if(pixel == 0):
                        pass
                    else:
                        fitness += 1
            self.fitness = fitness
            return fitness
                        
        m = math.tan(math.radians(self.angle))
        for x in range(self.width):
            y = int(m * (x - self.x) + self.y)
            if(y > 0 and y <self.height):

                if(y > 0 and y < self.height and x > 0 and x < self.width):
                    pixel = self.image[y, x]
                    if(pixel == 0):
                        pass
                    else:
                        fitness += 1
        self.fitness = fitness
        return fitness

    def mutate(self):
        mutation_factor = 10
        100/max(self.fitness, 0.01)
        self.x += random.randrange(-2, 2)
        self.y += random.randrange(-2, 2)
        self.angle += random.randrange(-int(max(mutation_factor, 4)), int(max(4,mutation_factor)))
        # print(self.angle)
        return self

    def showImage(self):

        m = math.tan(math.radians(self.angle))

        if(abs(self.angle - 90) < 0.1):

            x1 = self.x
            y1 = 0
            x2 = self.x
            y2 = height

        else:

            x1 = 0
            y1 = int(m * (x1 - self.x) + self.y)

            x2 = width
            y2 = int(m * (x2 - self.x) + self.y)

        temp_image = self.image.copy()

        if self.fitness == 0:
            masked_line = cv2.line(temp_image, (x1, y1), (x2, y2), (0, 0,255), 2)
        else:
            masked_line = cv2.line(temp_image, (x1, y1), (x2, y2), (0, 255,0), 2)

        font = cv2.FONT_HERSHEY_SIMPLEX
        fitness_message = str(self.fitness)
        
        
        cv2.putText(masked_line, fitness_message, (10, 100), font, 4, (255, 255, 255), 2, cv2.LINE_AA)
        cv2.imshow('image', masked_line)
        cv2.waitKey(0)

def evolve(Lines, winners):
    popSize = len(Lines)
    Lines.sort(key = lambda x:x.fitness, reverse=True)
    Lines = Lines[0:winners]
    for i in range(len(Lines)):
        line = Lines[i]
        for c in range(line.fitness):
            print(line.fitness, c)
            line.getFitness()
            Lines.append(line.mutate())

if __name__ == "__main__":
    """
    image_name = input("Enter the image name you want to analyse: ")
    img = cv2.imread(image_name)
    output_name = "output_" + image_name
    """

    img = cv2.imread("line2.jpg")
    img = cv2.resize(img, (0,0), fx=0.1, fy=0.1)
    image_name = "line2.jpg"
    output_name = "output_" + image_name

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    cv2.imwrite("hsv_" + image_name, hsv)

    lower_pink = np.array([50, 50, 220])
    upper_pink = np.array([254, 200, 255])

    mask = cv2.inRange(img, lower_pink, upper_pink)
    cv2.imwrite("pink_" + image_name, mask)

    height, width, channels = img.shape

    popSize = 100
    winners = 10
    epochs = 10

    lineList = []
    for x in range(popSize):
        lineList.append(Line(height, width, mask))
        # lineList[x].print()
        lineList[x].getFitness()
    
    for x in range(epochs):
        evolve(lineList, winners)
        lineList.sort(key = lambda x : x.fitness, reverse=True)
        lineList[0].showImage()

"""
    for x in range(epochs):
        for line in lineList:
            line.mutate(line, bestLine)
            line.getFitness()

    lineList.sort(key = lambda x:x.fitness, reverse = True)
    bestLine = lineList[0]

    bestLine.showImage()
"""




# TODO seed the "random  lines" with x, y that are on pink




    