import numpy as np 
import cv2
import math

def slope(x1,y1,x2,y2):
    top = y2-y1
    bot = x2-x1
    return top/bot

def length(x1,y1,x2,y2):
    yd = y2-y1
    xd = x2-x1
    return (yd**2+xd**2)**0.5

def intersect(x1,y1,x2,y2):
    return (y1-slope(x1,y1,x2,y2)*x1)

if(1):
    img = cv2.imread('18-LineDetection/line4.jpg')
    cv2.resize(img, (100,5))
    #cv2.imshow('image',img)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower_pink = np.array([50, 50, 220])
    upper_pink = np.array([254, 200, 255])
    mask = cv2.inRange(img, lower_pink, upper_pink)
    #cv2.imshow('hsv',mask)
    res = cv2.bitwise_and(img, img, mask = mask)
    edges = cv2.Canny(res, 50, 150)
    #cv2.imshow('edges',edges)
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 10)
    avgsum = 0.0
    slopesum = 0.0
    constantsum = 0.0
    for line in lines:
        x1, y1, x2, y2 = line[0]
        #print (slope(x1,y1,x2,y2),length(x1,y1,x2,y2),intersect(x1,y1,x2,y2))
        m = slope(x1,y1,x2,y2)
        b = intersect(x1,y1,x2,y2)
        l = length(x1,y1,x2,y2)
        if(l > 0 and not np.isnan(m) and not np.isinf(m)):
            avgsum += 1
            slopesum += m
            constantsum += b
         #   print(avgsum, slopesum, constantsum)
        cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 10)
    #print( avgsum, slopesum, constantsum)
    m = slopesum/avgsum
    b = constantsum/avgsum
    print(m,b)
    width, height, channels= img.shape
    print (height)
    x1 = int( -b/m)
    y1 = 0
    x2 = int(height*1/m-b/m)
    y2 = height
cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 10)
cv2.namedWindow('edges',cv2.WINDOW_NORMAL)
cv2.resizeWindow('edges', 1080,720)
cv2.imshow('edges', img)
cv2.imwrite('linefit.jpg', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
